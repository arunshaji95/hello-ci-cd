package main

import "testing"

func Test_hello(t *testing.T) {
	testCases := []struct {
		name     string
		expected string
	}{
		{
			name:     "Amanda",
			expected: "Hello, Amanda!",
		}, {
			name:     "Sunny",
			expected: "Hello, Sunny!",
		}, {
			name: "Rob",
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {

			got := hello(tt.name)
			if got != tt.expected {
				t.Fatalf("hello: expected %q, got %q", tt.expected, got)
			}
		})
	}
}
